# CodinGame Starter Project for Multi-Bot in Rust

This is my template for compete (multi) on [CodinGame](https://www.codingame.com) when I use rust lang.

The template can be used via [ffizer](https://github.com/davidB/ffizer)

```sh
cargo install ffizer

ffizer --destination my-bot --source https://gitlab.com/davidB31/cg-starter-multi-rust.git
```
