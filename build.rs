use std::path::Path;
extern crate rustsourcebundler;
use rustsourcebundler::Bundler;

fn main() {
    let mut bundler: Bundler = Bundler::from_main(Path::new("target/singlefile.rs"));
    // Bundler::new(Path::new("src/main.rs"), Path::new("target/singlefile.rs"));
    bundler.crate_name("starter");
    bundler.run();
    println!("run bundler");
}
