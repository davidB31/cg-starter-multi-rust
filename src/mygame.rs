use std::fmt;

macro_rules! parse_input {
    ($x:expr, $t:ident) => {
        $x.trim().parse::<$t>().unwrap()
    };
}

// ===============================================================================================
pub trait Commander {
    fn init(&mut self, states: &States);
    fn actions(&mut self, states: &States) -> Vec<Action>;
}

// ===============================================================================================
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub enum Action {
    NoAction{msg: String},
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Action::NoAction{msg} => write!(f, "WAIT {}", msg),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EntityKind {
    Undef,
}

impl From<i32> for EntityKind {
    fn from(v: i32) -> Self {
        EntityKind::Undef
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Entity {
    pub id: i32,
    pub kind: EntityKind,
    pub team: i32,
    //    pub pos: V2,
}

impl<'a> From<&'a String> for Entity {
    fn from(input_line: &String) -> Self {
        let inputs = input_line.split(' ').collect::<Vec<_>>();
        Entity {
            id: parse_input!(inputs[0], i32),
            kind: parse_input!(inputs[1], i32).into(),
            team: parse_input!(inputs[2], i32),
            /* pos: V2 {
             *     x: parse_input!(inputs[5], i32),
             *     y: parse_input!(inputs[6], i32),
             * }, */
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct States {
    pub round: i32,
    pub entities: Vec<Entity>,
    // pub scores: [i32; 4],
}

// ===============================================================================================
#[cfg(test)]
mod tests {
    use mygame::*;

    #[test]
    fn test_true() {
        assert_eq!(true, true);
    }
}
